/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.nasan.databaseproject.service;

import com.nasan.databaseproject.dao.UserDao;
import com.nasan.databaseproject.model.User;

/**
 *
 * @author nasan
 */
public class UserService {

    public User login(String name, String password) {
        UserDao userDao = new UserDao();
        User user = userDao.getByName(name);
        if (user.getPassword().equals(password)) {
            return user;
        }
        return null;
    }
}
